package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else {
            boolean result = true;
            if (x.equals(y)) return result;
            if (x.size() < y.size()) {
                int indexY = -1;
                for (int i = 0; i < x.size(); i++) {
                    result = false;
                    for (int j = 0; j < y.size(); j++) {
                        if (x.get(i).equals(y.get(j))) {
                            if (j > indexY) {
                                indexY = j;
                                result = true;
                                break;
                            } else {
                                result = false;
                            }
                        }
                    }
                    if (result == false) {
                        break;
                    }
                }
                return result;
            } else {
                result = false;
                return result;
            }
        }
    }
}
