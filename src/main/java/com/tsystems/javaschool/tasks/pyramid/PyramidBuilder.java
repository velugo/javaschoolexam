package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.size() > Integer.MAX_VALUE - 8) {  //максимальный размер массива не может быть больше Integer.MAX_VALUE - 8
            throw new CannotBuildPyramidException();
        }
        int[] mas = new int[inputNumbers.size()];
        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) != null) {
                mas[i] = inputNumbers.get(i);
            } else {
                throw new CannotBuildPyramidException();
            }
        }
        Arrays.sort(mas);
        int i = 1;                  //строка
        int length = mas.length;
        while (length > 0) {
            length -= i;
            if (length != 0) {
                i++;
            } else {
                break;
            }
        }
        if (length < 0) {
            throw new CannotBuildPyramidException();
        }
        int[][] pyram = new int[i][(i * 2) - 1];
        int idElMas = 0;
        int zeroCount1; //количество нулей в строке
        for (int n = 0; n < i; n++) {
            zeroCount1 = i - n - 1;
            int parseCount = n + 1; //количество чисел в строке
            boolean firstIn = true;
            for (int m = 0; m < (i * 2) - 1; m++) {
                if (zeroCount1 > 0) {
                    pyram[n][m] = 0;
                    pyram[n][i * 2 - 2 - m] = 0;
                    zeroCount1 --;
                } else {
                    if (parseCount > 0) {
                        if (m == 0) {
                            pyram[n][m] = mas[idElMas];
                            idElMas++;
                            parseCount--;
                            firstIn = false;
                        } else {
                            if (pyram[n][m - 1] == 0 || firstIn == true) {
                                pyram[n][m] = mas[idElMas];
                                idElMas++;
                                parseCount--;
                                firstIn = false;
                            } else {
                                pyram[n][m] = 0;
                            }
                        }
                    }
                }
            }
        }
        return pyram;
    }

}
